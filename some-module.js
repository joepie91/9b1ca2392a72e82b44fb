module.exports = function(configuration) {
	var configuredInstance = createInstance(configuration);
	
	return function(argOne, argTwo) {
		configuredInstance.doThing(argOne)
		return configuredInstance.doAnotherThing(argTwo);
	}
}